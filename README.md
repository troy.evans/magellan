# Welcome to the Magellan Project

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Getting The Application Started

To install any local dependencies.\

### `yarn install`

In the project directory, you should run both:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn api`

Runs the local API instance running on [http://localhost:3001](http://localhost:3001), you can view it in the browser.\
API endpoints:\
`/locations`\
`/products`

## Directions

After cloning this repo, please create a branch as your name `firstName-lastName` and make changes only to this branch. Please do not merge or pull from other branches inside the repository.

Please create a small application following these [designs](https://www.figma.com/file/jc9HnzoC9hr3UVn6Q3SACm/Dev-Training?node-id=0%3A1). The application must be mobile responsive and be as accurate as possible when styling. You can use what ever styling library of your choosing to get this done: css, scss/sass, material ui, etc. By hitting the local api endpoints stated above please create a list of locations in the left sidebar. The list of products, as cards, should be in the main panel.

### Sidebar

- The sidebar must contain the filter and the list of locations
- Each location must show the following:
  1. Products and their counts
  2. Products name and ID
- Must follow the designs
- Must be mobile responsive

#### Filtering

- The filter must function and filter as the input is being used
- Must filter by location name, and update the list based on the users input
- Clearing the filter updates the list to its initial state

#### Selector

- On initial load the selected location by default is the first one in the list
- After selecting the location on the left side the main panel is updated based on the selected location, the URL is updated to `/(selected location)`, the selected location is then the highlighted in the list.

#### In Mobile

- selecting the location in the list must open a drawer from the right side, and show the list of products
- this will not open a new page

### Main Panel
 
 - The main panel must contain the filters and the list of products as cards
 - The selected Location must be the title of the main panel
 - In each product you must show the following information:
  1. Product Name
  2. Suppliers and their counts
  3. Active state
- Must follow the designs
- Must be mobile responsive

#### Active State for Product

- Active State for product must be shown via a button that states Turn Off if active and Turn On if not active
- After selecting the active state button it must update that singular products active state

#### In Mobile

- Behavior does not change in mobile and everything must function as expected

### Other expectations

- Must use redux for state management and use at least 1 slice
- Must utilize react router to maintain the selected location in the sidebar
- Once you are finished please send the link to your code repository to Troy Evans via Slack